<?php

/**
 * @file
 * uw_ct_student_award.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_student_award_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management:admin/graduate-award-mass-publish.
  $menu_links['menu-site-management:admin/graduate-award-mass-publish'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/graduate-award-mass-publish',
    'router_path' => 'admin/graduate-award-mass-publish',
    'link_title' => 'Graduate award mass publish',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-site-management:admin/graduate-award-report.
  $menu_links['menu-site-management:admin/graduate-award-report'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/graduate-award-report',
    'router_path' => 'admin/graduate-award-report',
    'link_title' => 'Graduate award report',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Graduate award mass publish');
  t('Graduate award report');

  return $menu_links;
}
