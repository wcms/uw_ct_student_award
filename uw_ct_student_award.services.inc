<?php

/**
 * @file
 * uw_ct_student_award.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function uw_ct_student_award_default_services_endpoint() {
  $web_service_user = user_load_by_name('WCMS web service user');
  $web_service_user_id = $web_service_user->uid;
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'uwaterloo_grad_awards_v1';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/v1/grad_awards';
  $endpoint->authentication = array(
    'services_api_key_auth' => array(
      'api_key' => '96ab9383e6ad48c23aa1504dc9cc5c52',
      'user' => $web_service_user_id,
    ),
  );
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'grad_awards_feed' => array(
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['uwaterloo_grad_awards_v1'] = $endpoint;

  return $export;
}
