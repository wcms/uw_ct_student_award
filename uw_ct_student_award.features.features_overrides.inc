<?php

/**
 * @file
 * uw_ct_student_award.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_student_award_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_permission.
  $overrides["user_permission.access ckeditor link.roles|award content author"] = 'award content author';

  // Exported overrides for: variable.
  $overrides["variable.roleassign_roles.value|10"] = 10;
  $overrides["variable.roleassign_roles.value|11"] = 0;
  $overrides["variable.roleassign_roles.value|19"] = 0;

  return $overrides;
}
