<?php

/**
 * @file
 * uw_ct_student_award.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_student_award_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_award_notes'.
  $permissions['create field_award_notes'] = array(
    'name' => 'create field_award_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create uw_student_award content'.
  $permissions['create uw_student_award content'] = array(
    'name' => 'create uw_student_award content',
    'roles' => array(
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_student_award content'.
  $permissions['delete any uw_student_award content'] = array(
    'name' => 'delete any uw_student_award content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_student_award content'.
  $permissions['delete own uw_student_award content'] = array(
    'name' => 'delete own uw_student_award content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_student_award content'.
  $permissions['edit any uw_student_award content'] = array(
    'name' => 'edit any uw_student_award content',
    'roles' => array(
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit field_award_notes'.
  $permissions['edit field_award_notes'] = array(
    'name' => 'edit field_award_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_award_notes'.
  $permissions['edit own field_award_notes'] = array(
    'name' => 'edit own field_award_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own uw_student_award content'.
  $permissions['edit own uw_student_award content'] = array(
    'name' => 'edit own uw_student_award content',
    'roles' => array(
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_student_award revision log entry'.
  $permissions['enter uw_student_award revision log entry'] = array(
    'name' => 'enter uw_student_award revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_student_award authored by option'.
  $permissions['override uw_student_award authored by option'] = array(
    'name' => 'override uw_student_award authored by option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_student_award authored on option'.
  $permissions['override uw_student_award authored on option'] = array(
    'name' => 'override uw_student_award authored on option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_student_award promote to front page option'.
  $permissions['override uw_student_award promote to front page option'] = array(
    'name' => 'override uw_student_award promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_student_award published option'.
  $permissions['override uw_student_award published option'] = array(
    'name' => 'override uw_student_award published option',
    'roles' => array(
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_student_award revision option'.
  $permissions['override uw_student_award revision option'] = array(
    'name' => 'override uw_student_award revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_student_award sticky option'.
  $permissions['override uw_student_award sticky option'] = array(
    'name' => 'override uw_student_award sticky option',
    'roles' => array(
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'view field_award_notes'.
  $permissions['view field_award_notes'] = array(
    'name' => 'view field_award_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_award_notes'.
  $permissions['view own field_award_notes'] = array(
    'name' => 'view own field_award_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
