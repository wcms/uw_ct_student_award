/**
 * @file
 */

(function ($) {
   Drupal.behaviors.uw_ct_student_award = {
     attach: function (context, settings) {
        $("#edit-operation").find("option:contains('- Choose an operation -')").remove();
        $("#edit-state").find("option:contains('Draft')").remove();
        $("#edit-state").find("option:contains('Needs Review')").remove();
        $("#edit-state").find("option:contains('Archived')").remove();
        $("#edit-force-transition").hide();
     }
   };
}) (jQuery);
