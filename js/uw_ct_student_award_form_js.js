/**
 * @file
 */

(function ($) {
   $(function () {
      // Create a "select all programs" and a "deselect all programs" link-like span.
      $('#edit-field-award-programs-und').prepend('<p><span class="clickable-js" id="select-all-programs" title="select all programs"></span> <span class="clickable-js" id="deselect-all-programs" title="deselect all programs"></span></p>');
      // When the "select all programs" link-like span is clicked, check all the items and expand all the trees.
      $('#select-all-programs').click(function () {
         $('#edit-field-award-programs-und input').attr('checked','checked');
         $('#edit-field-award-programs-und .term-reference-tree-collapsed').click();
      });
      // When the "deselect all programs" link-like span is clicked, uncheck all the items and expand all the trees.
      $('#deselect-all-programs').click(function () {
         $('#edit-field-award-programs-und input').attr('checked','');
         $('#edit-field-award-programs-und .term-reference-tree-collapsed').click();
      });
      // Create parent terms "select all" and "deselect all" link-like spans.
      $('#edit-field-award-programs-und .parent-term').append(' <span class="clickable-js select-all" title="select all"></span> <span class="clickable-js deselect-all" title="deselect all"></span>');
      // When a "select all" link-like span is clicked, check all the items under this term and expand the tree if needed.
      $('#edit-field-award-programs-und .parent-term .select-all').click(function () {
         $('input',$(this).parent().next('ul')).attr('checked','checked');
         $('.term-reference-tree-collapsed',$(this).parents('li')).click();
      });
      // When a "deselect all" link-like span is clicked, uncheck all the items under this term and expand the tree if needed.
      $('#edit-field-award-programs-und .parent-term .deselect-all').click(function () {
         $('input',$(this).parent().next('ul')).attr('checked','');
         $('.term-reference-tree-collapsed',$(this).parents('li')).click();
      });

      // Create a "Select all types".
      $('#edit-field-scholarship-type-und').prepend('<div id="select-all-types"><input class="form-checkbox" type="checkbox" name="types" value="Select all types"  /> Select all types</div>');
      $('#select-all-types').click(function () {
         if ($('input:checkbox[name=types]').is(':checked')) {
           $('#edit-field-scholarship-type-und input').attr('checked',true);
         }
         else {
           $('#edit-field-scholarship-type-und input').attr('checked',false);
         }
      });

      // Create a "Select all status".
      $('#edit-field-citizenship-status-und').prepend('<div id="select-all-status"><input class="form-checkbox" type="checkbox" name="status" value="Select all status"  /> Select all status</div>');
      $('#select-all-status').click(function () {
         if ($('input:checkbox[name=status]').is(':checked')) {
           $('#edit-field-citizenship-status-und input').attr('checked',true);
         }
         else {
           $('#edit-field-citizenship-status-und input').attr('checked',false);
         }
      });

   });
}) (jQuery);
