<?php

/**
 * @file
 * uw_ct_student_award.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_student_award_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_student_award';
  $context->description = 'Displays graduate awards search on the sidebar';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'graduate-awards' => 'graduate-awards',
        'graduate-awards/*' => 'graduate-awards/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views--exp-student_award_search-page' => array(
          'module' => 'views',
          'delta' => '-exp-student_award_search-page',
          'region' => 'sidebar_second',
          'weight' => '-2',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays graduate awards search on the sidebar');
  $export['uw_student_award'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_student_award-home_page';
  $context->description = 'Displays search on  page graduate-funding-and-awards-database page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'awards-funding/database/graduate-funding-and-awards-database' => 'awards-funding/database/graduate-funding-and-awards-database',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views--exp-student_award_search-page' => array(
          'module' => 'views',
          'delta' => '-exp-student_award_search-page',
          'region' => 'content',
          'weight' => '9',
        ),
        'service_links-service_links' => array(
          'module' => 'service_links',
          'delta' => 'service_links',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays search on  page graduate-funding-and-awards-database page');
  $export['uw_student_award-home_page'] = $context;

  return $export;
}
