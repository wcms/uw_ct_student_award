<?php

/**
 * @file
 * uw_ct_student_award.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_student_award_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_student_award_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_user_default_permissions_alter().
 */
function uw_ct_student_award_user_default_permissions_alter(&$data) {
  if (isset($data['access ckeditor link'])) {
    $data['access ckeditor link']['roles']['award content author'] = 'award content author'; /* WAS: '' */
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_ct_student_award_strongarm_alter(&$data) {
  if (isset($data['roleassign_roles'])) {
    $data['roleassign_roles']->value[10] = 10; /* WAS: 0 */
    $data['roleassign_roles']->value[11] = 0; /* WAS: 11 */
    $data['roleassign_roles']->value[19] = 0; /* WAS: 19 */
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_student_award_node_info() {
  $items = array(
    'uw_student_award' => array(
      'name' => t('Graduate Award'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name of award'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
